<?php
/**
 * Template Name: Zorgverleners search
 *
 */

get_header();
?>

<div id="container" class="container center">

    <div class="content">
        <article>
            <div class="entry-content">
                <?php the_content(); ?>

		            <div class="searchheader">
		            	<h1><?php the_title(); ?></h1>
		            </div>
		            <div class="tableholder">
	                <table id="zorgverleners" class="tablesorter">
	                    <?php include 'includes/result_head.php'; ?>
	                    <tbody>
	                        <?php
	                        $args = array(
  	                        'post_type' => 'praktijk',
  	                        'posts_per_page' => -1,
  	                        'order' => ASC,
	                        );

	                          $the_query = new WP_Query( $args );

	                            while ( $the_query->have_posts() ) : $the_query->the_post();

	                        ?>

	                        <?php include 'includes/result_list.php'; ?>



	                        <?php
	                            endwhile;
	                            wp_reset_query();
	                        ?>
	                    </tbody>
	                </table>
		            </div>
            </div><!-- .entry-content -->
        </article>



    </div><!-- #content -->
</div><!-- container -->

<div class="container center single-praktijk--outer">
	<div style="clear:both;"></div>
    	<?php include 'includes/map.php'; ?>
    </div>
</div>



<?php get_footer(); ?>
