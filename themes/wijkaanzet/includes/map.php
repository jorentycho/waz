<?php
              $args = array(
                'post_type' => 'praktijk',
                'posts_per_page' => -1,
                'order' => ASC,
              );
                $markers = array();

                $the_query = new WP_Query( $args );

                  while ( $the_query->have_posts() ) : $the_query->the_post();

                  //use geocoder to get position

                  $address = get_field('straat_huisnummer').' '.get_field('postcode').' '.get_field('stad');
                  $position = geocode($address);

                  if($position) {
                  $website = get_field('website');
                  $phone = get_field('telefoon_praktijk');

                  }

                  $position_hash = md5($position[0].'-'.$position[1]);

                  $item = array(
                    'title' => get_the_title(),
                    'link' => get_the_permalink(),
                    'address' => $address,
                    'website' => $website,
                    'phone' => $phone
                  );

                  if(!$markers[$position_hash])
                    $markers[$position_hash] = array(
                      'position' => $position,
                      'items' => array($item)
                    );
                  else
                    array_push($markers[$position_hash]['items'], $item);

                  endwhile;
              ?>

<div class="mapholder">
    <div class="innermap">
        <h2>WAAR IN DEN HAAG ZUID-WEST</h2>

          <div class="acf-map">
              <?php foreach($markers as $m) { ?>
              <div class="marker" data-lat="<?php echo $m['position'][0]; ?>" data-lng="<?php echo $m['position'][1]; ?>">
                <?php foreach($m['items'] as $i) { ?>
                  <p class="address">
                    <strong><a style="font-size: 18px; color: #396F28 !important" href="<?php echo $i['link'] ?>"><?php echo $i['title'] ?></a></strong><br/>
                    <?php echo $i['address']; ?><br/>
                    <?php if($i['phone']) echo '<a href="tel:'.$i['phone'].'">'.$i['phone'].'</a>'; ?> <?php if($i['website']) { if($i['website']) echo ' | '; echo '<a href="'.$i['website'].'" target="_blank">'.$i['website'].'</a>'; } ?>
                  </p>
                <?php } ?>      
              </div>

              <?php
                }
              ?>
          </div><!-- .acf-map -->
    </div>
</div>
