<tr>
    <td class="c-1">
      <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
       <div class="m-640 no-desktop">
          <div class="address"><?php the_field('straat_huisnummer'); ?><br /><?php the_field('postcode'); ?> <?php the_field('stad'); ?></div>
          <div class="specialization"><?php the_field('type_zorginstelling'); ?></div>
        </div>

        <div class="m-1024 no-desktop">
          <div class="phone"><?php the_field('telefoon_praktijk'); ?></div>
          <div class="website"><a href="http://<?php the_field('website'); ?>" target="_blank"><?php the_field('website'); ?></a></div>
        <div>
    </td>
    <td class="c-2"><?php the_field('telefoon_praktijk'); ?></td>
    <td class="c-3"><?php the_field('straat_huisnummer'); ?><br /><?php the_field('postcode'); ?> <?php the_field('stad'); ?></td>

    <td class="c-4"><?php the_field('type_zorginstelling'); ?></td>
    <td class="c-5"><?php $link = addhttp( get_field('website') ); ?>
      <a href="<?php echo $link ?>" target="_blank"><?php the_field('website'); ?></a>
    <td class="c-6">
          <?php $term = get_field('samenwerkingsverband'); $ID = get_the_ID(); $term_list = wp_get_object_terms($ID, 'samenwerkingsverband'); ?>
          <?php foreach($term_list as $term_single) {
            echo '<a href="'.get_term_link($term_single).'">'.$term_single->name.'</a>';
          } ?>
    </td>
    <td class="c-7">
      <?php
        $patienten = get_field('neemt_nog_patienten_aan');
        if ($patienten == 'ja') {
            echo '<i class="ja fa fa-check"></i>';
        } else {
            echo '<i class="nee fa fa-times"></i>';
        }
      ?>
    </td>
</tr>
