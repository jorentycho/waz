<div class="searchbox">
    <h2><i class="fa fa-search"></i> VIND EEN PRAKTIJK</h2>

    <span class="label">Zoek op:</span>
    <span class="keyword tabclicker on">Vrij zoeken</span>
    <span class="praktijksoort tabclicker">Praktijksoort</span>
    <span class="specialisme tabclicker">Specialisme</span>

    <div style="clear:both;"></div>

    <div class="keywordzoeken tab on">
        <form method="get" id="zoekenkeyword" action="<?php echo get_site_url(); ?>">
            <div class="styled-text">
              <input required class="zoek" type="text" placeholder="Voer zoekterm in" value="<?php echo wp_specialchars($s, 1); ?>" name="s" id="s" />
            </div>
            <input  type="submit" id="zoeksubmit" value="Zoeken" class="btn" />

            <div style="clear:both;"></div>

        </form>
    </div>



    <div class="praktijkzoeken tab">
        <form method="get" id="zoekenpraktijk" action="<?php echo get_site_url(); ?>">
            <div class="styled-select">

            <?php
              $field_key = "field_584a843f9a63b";
              $field = get_field_object($field_key);

              if( $field )
              {
                echo '<select name="s" id="type">';
                    echo '<option disabled selected value>Kies praktijksoort</option>';
                  foreach( $field['choices'] as $k => $v )
                  {
                    echo '<option value="' . $k . '">' . $v . '</option>';
                  }
                echo '</select>';
              }
            ?>


            </div>
            <input type="submit" id="praktijksubmit" value="Zoeken" />

            <div style="clear:both;"></div>

        </form>
    </div>

    <div class="specialismezoeken tab">
      <?php
        $beroep_key = "field_584a8582b79e7";
        $beroep = get_field_object($beroep_key);
        ?>

        <?php // dit is 2de selectbox welke gevuld/populated wordt met waardes via javascript/ajax ?>
        <form role="search" method="get" id="searchform" action="<?php echo get_site_url(); ?>">
          <div>
            <div class="styled-select">
                <?php if( $beroep )
                {
                  echo '<select name="s" id="stype">';
                      echo '<option disabled selected value>Kies beroep</option>';
                    foreach( $beroep['choices'] as $k => $v )
                    {

                      echo '<option value="' . $k . '">' . $v . '</option>';
                    }
                  echo '</select>';
                } ?>
            </div>

            <div class="styled-select">
              <select name="subtype" id="subtype">
               <option disabled selected value>Kies zorgverlener</option>
              </select>
            </div>

            <input type="submit" id="searchsubmit" value="Zoeken" />

            <div style="clear:both;"></div>

          </div>

        </form>
    </div>

</div>
