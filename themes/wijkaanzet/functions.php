<?php

/*
  ****************** ACF ******************
*/

    function theme_prefix_setup() {

        add_theme_support( 'custom-logo', array(
            'height'      => 84,
            'width'       => 221,
            'flex-width' => true,
        ) );

    }
    add_action( 'after_setup_theme', 'theme_prefix_setup' );

    // add_action( 'init', 'acf_init' );
    // function acf_init() {
    //   // Import ACF
    //   //define( 'ACF_LITE' , false );
    //
    //   // Rename options page and register subpages
    //   if( function_exists('acf_add_options_page') ) {
    //       $page = array(
    //
    //         'page_title' => 'Instellingen',
    //         'menu_title' => '',
    //         'menu_slug' => '',
    //         'position' => '99.1',
    //         'parent_slug' => '',
    //         'icon_url' => false,
    //         'redirect' => true,
    //         'post_id' => 'options',
    //         'autoload' => false,
    //
    //       );
    //       acf_add_options_page($page);
    //
    //   }
    //
    //   if( function_exists('acf_set_options_page_capability') ) {
    //       acf_set_options_page_capability('read');
    //   }
    // }

    /*
      ****************** Register post types ******************
    */


        // Register Producten Post Type
        function eoffice_producten_cpt()
        {
            $labels = array(
                'name'                  => _x('Praktijken', 'Post Type General Name', 'text_domain'),
                'singular_name'         => _x('Praktijk', 'Post Type Singular Name', 'text_domain'),
                'menu_name'             => __('Praktijken', 'text_domain'),
                'name_admin_bar'        => __('Praktijken', 'text_domain'),
                'archives'              => __('Praktijken archief', 'text_domain'),
                'parent_item_colon'     => __('Parent praktijk:', 'text_domain'),
                'all_items'             => __('Alle praktijken', 'text_domain'),
                'add_new_item'          => __('Voeg praktijk toe', 'text_domain'),
                'add_new'               => __('Voeg nieuwe toe', 'text_domain'),
                'new_item'              => __('Nieuwe praktijk', 'text_domain'),
                'edit_item'             => __('Bewerk praktijk', 'text_domain'),
                'update_item'           => __('Werk praktijk bij', 'text_domain'),
                'view_item'             => __('Bekijk praktijk', 'text_domain'),
                'search_items'          => __('Zoek praktijken', 'text_domain'),
                'not_found'             => __('Niets gevonden', 'text_domain'),
                'not_found_in_trash'    => __('Niets gevonden', 'text_domain'),
                'featured_image'        => __('Featured Image', 'text_domain'),
                'set_featured_image'    => __('Set featured image', 'text_domain'),
                'remove_featured_image' => __('Remove featured image', 'text_domain'),
                'use_featured_image'    => __('Use as featured image', 'text_domain'),
                'insert_into_item'      => __('Insert into item', 'text_domain'),
                'uploaded_to_this_item' => __('Uploaded to this item', 'text_domain'),
                'items_list'            => __('Items list', 'text_domain'),
                'items_list_navigation' => __('Items list navigation', 'text_domain'),
                'filter_items_list'     => __('Filter items list', 'text_domain'),
            );
            $args = array(
                'label'                 => __('Praktijken', 'text_domain'),
                'description'           => __('Praktijken', 'text_domain'),
                'labels'                => $labels,
                'supports'              => array('title', 'editor', 'thumbnail', 'revisions' ),
                'taxonomies'            => array('samenwerkingsverband'),
                'hierarchical'          => false,
                    'public'                => true,
                    'show_ui'               => true,
                    'show_in_menu'          => true,
                    'menu_position'         => 5,
                    'show_in_admin_bar'     => false,
                    'show_in_nav_menus'     => false,
                    'can_export'            => true,
                    'has_archive'           => false,
                    'exclude_from_search'   => false,
                    'publicly_queryable'    => true,
                    'capability_type'       => 'page'
            );
            register_post_type('praktijk', $args);

                    }
        add_action('init', 'eoffice_producten_cpt', 0);


    /*
      ****************** Register taxonomies ******************
    */
        // Product groups
        function create_huiswerk_modules()
        {
            register_taxonomy(
                'samenwerkingsverband',
                'praktijk',
                array(
                    'label' => 'Samenwerkingsverband',
                    'labels' => array(
                        'add_new_item' => 'Nieuw samenwerkingsverband',
                        'search_items' => 'Samenwerkingsverbanden zoeken',
                        'menu_name' => 'Samenwerkingsverbanden',
                        'all_items' => 'Alle samenwerkingsverbanden',

                    ),
                    'rewrite' => array( 'slug' => 'samenwerkingsverbanden' ),
                    'hierarchical' => true,
                    'show_in_menu' => true,
                    'show_ui' => true,
                    'show_in_nav_menus' => true,
                    'show_admin_column' => true,
                    'show_in_quick_edit' => true,

                )
            );

        }
        add_action('init', 'create_huiswerk_modules');



/*
  ****************** Navigation ******************
*/

    // Register nav menu's
    add_action( 'after_setup_theme', 'navigation_registration' );
    function navigation_registration() {
      register_nav_menu( 'primary', __( 'Top menu', 'primary' ) );
      register_nav_menu( 'footer', __( 'Footer menu', 'footer' ) );
      register_nav_menu( 'minifooter', __( 'Footer disclaimer privacy contact', 'minifooter' ) );
    }



/*
  ****************** Set terms on publish ******************
*/

    add_action('admin_head', 'my_custom_fonts');

    function my_custom_fonts() {
      echo '<style>
        #samenwerkingsverbanddiv {
          display:none;
        }
      </style>';
    }



    function ps_acf_save_post( $post_id ) {
        // Don't do this on the ACF post type
        if ( get_post_type( $post_id ) == 'acf' ) return;

        // Get the Fields
        $fields = get_field_objects( $post_id );

        // Prevent Infinite Looping...
        remove_action( 'acf/save_post', 'my_acf_save_post' );

        // Grab Post Data from the Form
        $post = array(
            'ID'           => $post_id,
            'post_type'    => 'praktijk',
            'post_title'   => $fields['naam_zorgpraktijk_of_zorginstelling']['value'],
        );

        // Update the Post
        wp_update_post( $post );

        $swvb = get_field('samenwerkingsverband', $post_id);
        wp_set_object_terms($post_id, $swvb, 'samenwerkingsverband', false );

        // Continue save action
        add_action( 'acf/save_post', 'my_save_post' );

        // Set the Return URL in Case of 'new' Post
        $_POST['return'] = add_query_arg( 'updated', 'true', get_permalink( $post_id ) );
    }
    add_action( 'acf/save_post', 'ps_acf_save_post', 10, 1 );



/*
  ****************** Remove support ******************
*/

    if ( ! function_exists('solarmade_cleaner_wordpress') ) {      function solarmade_cleaner_wordpress() {
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'feed_links', 2);
        remove_action('wp_head', 'feed_links_extra', 3);
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'start_post_rel_link', 10, 0);
        remove_action('wp_head', 'parent_post_rel_link', 10, 0);
        remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
        remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 );
        remove_action( 'admin_print_styles', 'print_emoji_styles' );
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
        remove_action( 'wp_print_styles', 'print_emoji_styles' );
        remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
        remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
        remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

        // Added by hand
        add_filter( 'emoji_svg_url', '__return_false' );
        remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
        remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
      }
      add_action( 'init', 'solarmade_cleaner_wordpress' );

      //hide admin bar on frontend
      add_filter('show_admin_bar', '__return_false');
    }

/*
  ****************** Extra's ******************
*/
    function wpdocs_theme_name_scripts() {
        wp_enqueue_style( 'fontAawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' );
        wp_enqueue_script( 'tablesorter', '/wp-content/themes/wijkaanzet/js/jquery.tablesorter.js', array('jquery') );
        wp_enqueue_script( 'main', '/wp-content/themes/wijkaanzet/js/main.js', array('jquery') );
    }

    // Add theme support
    function custom_stuff() {
      // post thumbnail
      add_theme_support( 'post-thumbnails' );

      // html5 searchform
      add_theme_support( 'html5', array( 'search-form' ) );
    }
    add_action( 'after_setup_theme', 'custom_stuff' );

    add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

    // Add SVG support for media uploader
    function cc_mime_types($mimes) {
      $mimes['svg'] = 'image/svg+xml';
      return $mimes;
    }
    add_filter('upload_mimes', 'cc_mime_types');


    // Change Excerpt length
    function custom_excerpt_length( $length ) {
      return 55;
    }
    add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


    // Change Excerpt ending
    function new_excerpt_more( $more ) {
      return '';
    }
    add_filter('excerpt_more', 'new_excerpt_more');

    // add http:// if it's not exists in the URL
    function addhttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }


    // new post type



         // custom post type 'categories' voor custom post type zorgverleners
        add_action( 'init', 'build_taxonomies', 0 );

        function build_taxonomies() {
            register_taxonomy(
                'categories',
                'zorgverleners',
                array (
                    'label' => 'Categories',
                    'query_var' => true,
                    'rewrite' =>  array('slug' => 'zorgverleners' ),
                    'show_ui' => true,
                    'show_admin_column' => true,
                    'show_tagcloud' => false,
                    'hierarchical' => true

                )
            );
        }

        /*add category to the permalink*/
        add_filter('post_link', 'brand_permalink', 1, 3);
        add_filter('post_type_link', 'brand_permalink', 1, 3);

        function brand_permalink($permalink, $post_id, $leavename) {
            //con %brand% catturo il rewrite del Custom Post Type
            if (strpos($permalink, '%categories%') === FALSE) return $permalink;
                // Get post
                $post = get_post($post_id);
                if (!$post) return $permalink;

                // Get taxonomy terms
                $terms = wp_get_object_terms($post->ID, 'categories');
                if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0]))
                    $taxonomy_slug = $terms[0]->slug;
                else $taxonomy_slug = 'zorgverleners';

            return str_replace('%categories%', $taxonomy_slug, $permalink);
        }



    /////////////////////////////////////////////
    // dropdown based on selected maincategory //
    /////////////////////////////////////////////

    add_action('wp_ajax_nopriv_my_special_action', 'my_action_callback');


    function implement_ajax() {
          if($_POST['action'] == 'types'  && isset($_POST['type']))
                {
                  $args = array(
             'title'        => $_POST['type'],
             'post_type'   => 'acf-field',
             'post_status' => 'publish',
             'numberposts' => 1
            );


            $field = array_shift(get_posts($args));

            $key = $field->post_name;
            $types = get_field_object($key);

            $options = '<option value="-1">Kies zorgverlener</option>';
            foreach( $types['choices'] as $k => $v )
            {
             $options .= '<option value="' . $k . '">' . $v . '</option>';
            }

                  echo $options;
                  exit();
                }
    }
    add_action('wp_ajax_types', 'implement_ajax');
    add_action('wp_ajax_nopriv_types', 'implement_ajax');//for users that are not logged in.


    //////////////////////////////
    // search all custom fields //
    //////////////////////////////
    function cf_search_join( $join ) {
        global $wpdb;

        if ( is_search() ) {
            $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
        }

        return $join;
    }
    add_filter('posts_join', 'cf_search_join' );

    /**
     * Modify the search query with posts_where
     *
     * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
     */
    function cf_search_where( $where ) {
        global $pagenow, $wpdb;

        if ( is_search() ) {
            $where = preg_replace(
                "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
                "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
        }

        $where = str_replace('0 = 1', '1', $where);

        return $where;
    }
    add_filter( 'posts_where', 'cf_search_where' );

    /**
     * Prevent duplicates
     *
     * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
     */
    function cf_search_distinct( $where ) {
        global $wpdb;

        if ( is_search() ) {
            return "DISTINCT";
        }

        return $where;
    }
    add_filter( 'posts_distinct', 'cf_search_distinct' );


    // =========================================================================
  // Custom duplicate post type
  // https://rudrastyh.com/wordpress/duplicate-post.html
  // =========================================================================
  /*
   * Function creates post duplicate as a draft and redirects then to the edit post screen
   */
  function rd_duplicate_post_as_draft(){
    global $wpdb;
    if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
      wp_die('No post to duplicate has been supplied!');
    }

    /*
     * get the original post id
     */
    $post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
    /*
     * and all the original post data then
     */
    $post = get_post( $post_id );

    /*
     * if you don't want current user to be the new post author,
     * then change next couple of lines to this: $new_post_author = $post->post_author;
     */
    $current_user = wp_get_current_user();
    $new_post_author = $current_user->ID;

    /*
     * if post data exists, create the post duplicate
     */
    if (isset( $post ) && $post != null) {

      /*
       * new post data array
       */
      $args = array(
        'comment_status' => $post->comment_status,
        'ping_status'    => $post->ping_status,
        'post_author'    => $new_post_author,
        'post_content'   => $post->post_content,
        'post_excerpt'   => $post->post_excerpt,
        'post_name'      => $post->post_name,
        'post_parent'    => $post->post_parent,
        'post_password'  => $post->post_password,
        'post_status'    => 'draft',
        'post_title'     => $post->post_title,
        'post_type'      => $post->post_type,
        'to_ping'        => $post->to_ping,
        'menu_order'     => $post->menu_order
      );

      /*
       * insert the post by wp_insert_post() function
       */
      $new_post_id = wp_insert_post( $args );

      /*
       * get all current post terms ad set them to the new post draft
       */
      $taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
      foreach ($taxonomies as $taxonomy) {
        $post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
        wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
      }

      /*
       * duplicate all post meta just in two SQL queries
       */
      $post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
      if (count($post_meta_infos)!=0) {
        $sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
        foreach ($post_meta_infos as $meta_info) {
          $meta_key = $meta_info->meta_key;
          $meta_value = addslashes($meta_info->meta_value);
          $sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
        }
        $sql_query.= implode(" UNION ALL ", $sql_query_sel);
        $wpdb->query($sql_query);
      }


      /*
       * finally, redirect to the edit post screen for the new draft
       */
      wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
      exit;
    } else {
      wp_die('Post creation failed, could not find original post: ' . $post_id);
    }
  }
  add_action( 'admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft' );

  /*
   * Add the duplicate link to action list for post_row_actions
   */
  function rd_duplicate_post_link( $actions, $post ) {
    if (current_user_can('edit_posts')) {
      $actions['duplicate'] = '<a href="admin.php?action=rd_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
    }
    return $actions;
  }

  add_filter('post_row_actions', 'rd_duplicate_post_link', 10, 2);

  // confirm email field

  add_filter( 'wpcf7_validate_email*', 'custom_email_confirmation_validation_filter', 20, 2 );

function custom_email_confirmation_validation_filter( $result, $tag ) {
    $tag = new WPCF7_FormTag( $tag );

    if ( 'your-email-confirm' == $tag->name ) {
        $your_email = isset( $_POST['your-email'] ) ? trim( $_POST['your-email'] ) : '';
        $your_email_confirm = isset( $_POST['your-email-confirm'] ) ? trim( $_POST['your-email-confirm'] ) : '';

        if ( $your_email != $your_email_confirm ) {
            $result->invalidate( $tag, "De e-mailadressen komen niet met elkaar overeen" );
        }
    }

    return $result;
}


    //////////////////////////
    // search all taxonomies//
    //////////////////////////
    

    //Remove news and other pages from search results
    function SearchFilter($query) {
    if ($query->is_search) {
    $query->set('post_type', 'praktijk');
    }
    return $query;
    }

    add_filter('pre_get_posts','SearchFilter');

    function atom_search_where($where){
        global $wpdb, $wp_query;
        if (is_search()) {
            $search_terms = get_query_var( 'search_terms' );

            $where .= " OR (";
            $i = 0;
            foreach ($search_terms as $search_term) {
                $i++;
                if ($i>1) $where .= " AND";     // --- make this OR if you prefer not requiring all search terms to match taxonomies
                $where .= " (t.name LIKE '%".$search_term."%')";
            }
            $where .= " AND {$wpdb->posts}.post_status = 'publish')";
        }
      return $where;
    }

    function atom_search_join($join){
      global $wpdb;
      if (is_search())
        $join .= "LEFT JOIN {$wpdb->term_relationships} tr ON {$wpdb->posts}.ID = tr.object_id LEFT JOIN {$wpdb->term_taxonomy} tt ON tt.term_taxonomy_id=tr.term_taxonomy_id LEFT JOIN {$wpdb->terms} t ON t.term_id = tt.term_id";
      return $join;
    }

    function atom_search_groupby($groupby){
      global $wpdb;

      // we need to group on post ID
      $groupby_id = "{$wpdb->posts}.ID";
      if(!is_search() || strpos($groupby, $groupby_id) !== false) return $groupby;

      // groupby was empty, use ours
      if(!strlen(trim($groupby))) return $groupby_id;

      // wasn't empty, append ours
      return $groupby.", ".$groupby_id;
    }

    add_filter('posts_where','atom_search_where');
    add_filter('posts_join', 'atom_search_join');
    add_filter('posts_groupby', 'atom_search_groupby');
    remove_filter('template_redirect', 'redirect_canonical');

    function modify_read_more_link() {
        return '<a class="more-link" href="' . get_permalink() . '">lees verder</a>';
    }
    add_filter( 'the_content_more_link', 'modify_read_more_link' );


    add_action( 'template_redirect', 'zorgverlener_redirect' );
    function zorgverlener_redirect() {
        global $post;
        global $wp;

        if ( FALSE !== strpos($_SERVER['REQUEST_URI'],'/praktijk') ) {
            $parts = explode('/praktijk',$_SERVER['REQUEST_URI']);
            unset($parts[0]);
            $parts = explode('/',implode('/praktijk',$parts));

            if(count($parts) == 3 && $parts[1]) {
              $option = array(
                'posts_per_page' => -1,
                'post_type' => 'praktijk',
                'name' => sanitize_title($parts[1])
              );

              $new_query = new WP_Query($option);

              if ($new_query->have_posts()) {
                while ($new_query->have_posts()) {
                  $new_query->the_post();
                  $praktijk = $post;

                  if(have_rows('zorgverleners')) {

                              while( have_rows('zorgverleners') ): the_row();
                                  $lastname = explode(' ',get_sub_field('naam_openbaar'));
                                  if($lastname) {
                                    $lastname = $lastname[count($lastname) -1];
                                    $slug = sanitize_title($lastname);

                                    if($slug == $parts[2]) {
                                      $template = get_template_directory().'/single-zorgverlener.php';



                                      include($template);
                                      exit();
                                    }
                                  }
                              endwhile;
                  }
                }
              }
            }
        }
    }


    //function to geocode address, it will return false if unable to geocode address
    function geocode($address){

        // url encode the address
        $address = urlencode($address);

        // google map geocode api url
        $key = 'AIzaSyDpob_3o6rBKzhmBEGlNqiqdjsl8VEMLPw';
        $url = "https://maps.google.com/maps/api/geocode/json?address={$address}&key={$key}";

        // get the json response
        $resp_json = file_get_contents($url);

        // decode the json
        $resp = json_decode($resp_json, true);

        // response status will be 'OK', if able to geocode given address
        if($resp['status']=='OK'){

            // get the important data
            $lati = $resp['results'][0]['geometry']['location']['lat'];
            $longi = $resp['results'][0]['geometry']['location']['lng'];
            $formatted_address = $resp['results'][0]['formatted_address'];

            // verify if data is complete
            if($lati && $longi && $formatted_address){

                // put the data in the array
                $data_arr = array();

                array_push(
                    $data_arr,
                        $lati,
                        $longi,
                        $formatted_address
                    );

                return $data_arr;

            }else{
                return false;
            }

        }else{
            return false;
        }
    }
?>
