<?php get_header(); ?>

    <div class="container center">
    <a href="<?php the_permalink(); ?>"><span class="backbutton">Terug</span></a>
        <div class="content single_zorgverlener">
            <article>
                <div class="entry-content">
                    <div class="searchheader">
                        <h1><?php the_sub_field('naam_openbaar'); ?></h1>
                    </div>
                    <div class="colholder sameheight">
                       <div id="zorgverleners" class="zorgverlener-intro padding">
                           <p>Beroep: 

                           <?php $beroep = get_sub_field('beroep'); ?>

                           <?php if ($beroep == 'Apotheker') {
                            echo Apotheker;
                           } elseif ($beroep == 'Diëtist') {
                            echo Diëtist;
                           } elseif ($beroep == 'Ergotherapeut') {
                            echo Ergotherapeut;
                           } elseif ($beroep == 'Fysiotherapeut') {
                            echo Fysiotherapeut;
                           } elseif ($beroep == 'Huisarts') {
                            echo Huisarts;
                           } elseif ($beroep == 'Jeugdarts') {
                            echo Jeugdarts;
                           } elseif ($beroep == 'Logopedist') {
                            echo Logopedist;
                           } elseif ($beroep == 'Mondhygiënist') {
                            echo Mondhygiënist;
                           } elseif ($beroep == 'Oefentherapeut') {
                            echo Oefentherapeut;
                           } elseif ($beroep == 'Optometrist') {
                            echo Optometrist;
                           } elseif ($beroep == 'Orthodontist') {
                            echo Orthodontist;
                           } elseif ($beroep == 'Orthopedagoog') {
                            echo Orthopedagoog;
                           } elseif ($beroep == 'Ouderenconsulent') {
                            echo Ouderenconsulent;
                           } elseif ($beroep == 'Podoloog') {
                            echo Podoloog;
                           } elseif ($beroep == 'Psychiater') {
                            echo Psychiater;
                           } elseif ($beroep == 'Psycholoog') {
                            echo Psycholoog;
                           } elseif ($beroep == 'Psychotherapeut') {
                            echo Psychoterapeut;
                           } elseif ($beroep == 'Specialist Ouderengeneeskunde') {
                            echo 'Specialist' + 'Ouderengeneeskunde';
                           } elseif ($beroep == 'Tandarts') {
                            echo Tandarts;
                           } elseif ($beroep == 'Tandprotheticus') {
                            echo Tandprotheticus;
                           } elseif ($beroep == 'Verloskundige') {
                            echo Verloskundige;
                           } elseif ($beroep == 'Coördinator Jeugdteam') {
                            echo Coördinator;
                           } elseif ($beroep == 'Coördinator Sociaal wijk-zorgteam') {
                            echo Coördinator;
                           } elseif ($beroep == 'Maatschappelijk werker') {
                            echo Maatschappelijk;
                           } elseif ($beroep == 'Schoolverpleegkundige') {
                            echo Schoolverpleegkundige;
                           } elseif ($beroep == 'Wijkverpleegkundige') {
                            echo Wijkverpleegkundige;
                           } elseif ($beroep == 'Overig') {
                            echo Overig;
                           }


                           ?>

                           </p>
                           <?php $beroep = get_sub_field('beroep'); ?>

                           <?php if ( $beroep == 'Ergotherapeut' || $beroep == 'Fysiotherapeut' || $beroep == 'Logopedist' || $beroep == 'Oefentherapeut' || $beroep == 'Psychiater' || $beroep == 'Psycholoog' || $beroep == 'Psychotherapeut' || $beroep == 'Tandarts' ): ?>
                             
                             <p>Specialisatie:
                               <span class="specialisatieveld">
                                 
                                 <?php if ($beroep == 'Ergotherapeut') {
                                  the_sub_field('ergotherapeut');
                                 } elseif ($beroep == 'Fysiotherapeut') {
                                  the_sub_field('fysiotherapeut');
                                 } elseif ($beroep == 'Logopedist') {
                                  the_sub_field('logopedist');
                                 } elseif ($beroep == 'Oefentherapeut') {
                                  the_sub_field('oefentherapeut');
                                 } elseif ($beroep == 'Psychiater') {
                                  the_sub_field('psychiater');
                                 } elseif ($beroep == 'Psycholoog') {
                                  the_sub_field('psycholoog');
                                 } elseif ($beroep == 'Psychotherapeut') {
                                  the_sub_field('psychoterapeut');
                                 } elseif ($beroep == 'Tandarts') {
                                  the_sub_field('tandarts');
                                 }
                                 ?>

                               </span>
                             </p>
                           <?php endif ?>
                           
                           <p>Zorginstelling: <?php the_field('naam_zorgpraktijk_of_zorginstelling'); ?></p>
                       </div>
                    </div>

                    <div class="col-4 col-d-12 left">
                      <div class="zorgverlener-info info padding">
                        <p><strong>TELEFOON</strong>
                        <?php the_sub_field('telefoon_praktijk_openbaar'); ?></p>
                        <p><strong>ADRES</strong>
                        <?php the_sub_field('adres_praktijk_openbaar'); ?></p>
                        <p><strong>TELEFOON PRAKTIJK</strong>
                        <?php the_sub_field('telefoon_praktijk_openbaar'); ?></p>
                        <p><strong>E-MAIL PRAKTIJK</strong>
                        <a href="mailto:<?php the_sub_field('e-mail_praktijk_openbaar'); ?>"><?php the_sub_field('e-mail_praktijk_openbaar'); ?></a></p>
                      </div>
                    </div>
                    <div class="col-4 col-d-12 left">
                    <div class="zorgverlener-tekst padding">
                      <?php the_sub_field('extra_tekstruimte'); ?></p>
                    </div>
                    </div>
                    <div class="col-4 col-d-12 left">
                      <div class="zorgverlener-foto padding">
                        <img src="<?php the_sub_field('profielfoto'); ?>" alt="">
                      </div>
                    </div>
                </div><!-- .entry-content -->
            </article>
        </div><!-- #content -->
    </div>

    <div class="container center single-praktijk--outer">
      <div class="mapholder padding">
        <div class="row">
          <div class="innermap">
              <h2>WAAR IN DEN HAAG ZUID-WEST</h2>

                <div class="acf-map">

                    <?php

                        //use geocoder to get position

                        $address = get_field('straat_huisnummer').' '.get_field('postcode').' '.get_field('stad');
                        $position = geocode($address);

                        if($position) {
                        $website = get_field('website');
                        $phone = get_field('telefoon_praktijk');
                    ?>


                    <div class="marker" data-lat="<?php echo $position[0]; ?>" data-lng="<?php echo $position[1]; ?>">
                            <p class="address">
                              <strong><a style="font-size: 18px; color: #396F28 !important" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></strong><br/>
                              <?php echo $address; ?><br/>
                              <?php if($phone) echo '<a href="tel:'.$phone.'">'.$phone.'</a>'; ?> <?php if($website) { if($phone) echo ' | '; echo '<a href="'.$website.'">'.$website.'</a>'; } ?>
                            </p>
                    </div>

                    <?php
                        }
                        wp_reset_query();
                    ?>
                </div><!-- .acf-map -->
          </div>
        </div>
      </div>
    </div>




<?php get_footer(); ?>
