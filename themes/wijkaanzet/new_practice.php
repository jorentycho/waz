<?php
/**
 * Template Name: New practice
 *
 */

acf_form_head();
get_header();
?>
<?php get_header(); ?>

<?php if(!$_GET['updated']): ?>
	<div class="container center">
		<div class="col-12 col-t-12 col-m-12 left right-t left-m padding">
				<div class="form--practice">
					<?php while ( have_posts() ) : the_post(); ?>
						<h1 class="title"><?php the_title(); ?></h1>
						<?php acf_form(array(
							'post_id'		=> 'new_post',
							'post_title'	=> false,
							'post_content'	=> false,
							'new_post'		=> array(
								'post_type'		=> 'praktijk',
								'post_status'		=> 'draft'
							),
							'submit_value'		=> 'Versturen'
						)); ?>

					<?php endwhile; ?>
				</div>
		</div>
	</div>
<?php else: ?>

	<div id="primary">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<div class="container center padding">
					<h1><?php the_title(); ?></h1>

					<div class="columns">
						<?php the_content(); ?>
					</div>
				</div>

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->
	
<?php endif; ?>



<?php get_footer(); ?>
