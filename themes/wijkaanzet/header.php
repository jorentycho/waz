<!doctype html>

<html>
<head>
	<title><?php bloginfo('title' ); ?></title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/style.css">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <div class="line--top"></div>
  <div class="container center">

    <a href="<?php echo bloginfo('url') ?>"><img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/waz-logo.jpg" /></a>

    <?php if ( has_nav_menu( 'primary' ) ) : ?>
      <h3 class="menu-toggle"><i class="fa fa-bars toggleicon"></i></h3>

      <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
        <?php
          wp_nav_menu( array(
            'theme_location' => 'primary',
            'menu_class'     => 'primary-menu',
           ) );
        ?>
      </nav><!-- .main-navigation -->
    <?php endif; ?>
  </div>
  <div class="line--bottom"></div>
	<?php if(get_the_post_thumbnail_url() ): ?>
		<div class="container center container--header" style="background-image: url(<?php echo( get_the_post_thumbnail_url() ); ?>)">
	<?php else: ?>
		<div class="container center container--header" style="background-image: url(<?php echo( get_stylesheet_directory_uri() ); ?>/images/header-pica.png)">

	<?php endif; ?>


  </div>
	<header>

	</header>
