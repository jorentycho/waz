<?php get_header(); ?>

	<div id="primary">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<div class="container center padding">
					<h1><?php the_title(); ?></h1>

					<div class="columns">
						<?php the_content(); ?>
					</div>
				</div>

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
