<?php 	$emptysearch = $_GET['s'];

if (empty($_GET['s'])) {
			$emptysearch = '-1';

}


		if ($emptysearch == '-1' ) {


      header("Location: /", true, 301);

		}
?>

<?php
get_header();
?>
<div id="container" class="container center">

    <div class="content">
        <article>
            <div class="entry-content">
                <?php the_content(); ?>

		            <div class="searchheader">
		            	<h2>ZOEKRESULTATEN</h2>
                  <h3>Zoekresultaten voor: <span>

                    <?php
                    printf( __( '%s', 'twentyfifteen' ), get_search_query() );
                    if(isset($_GET['subtype'])) echo ' '.$_GET['subtype'];
                    ?>

                  </span></h3>
		            </div>
		            <div class="tableholder">
                    <table id="zorgverleners" class="tablesorter" cellpadding="0" cellspacing="0">
                      <?php include 'includes/result_head.php'; ?>
                      <tbody>

												<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
													<?php $subtype = $_GET['subtype'];
	                        $type = $_GET['s'];

	                        if($subtype) {
	                          $stype = get_field('zorgverleners_0_'.strtolower(sanitize_title($type)));
		                      }


	                        if(($stype && $stype[0] == $subtype) || !$stype) {

														include 'includes/result_list.php';

													} ?>


													<?php endwhile; else : ?>
														<tr>
															<td colspan="7">
																Er zijn geen resultaten gevonden.
															</td>
														</tr>
													<?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- .entry-content -->
        </article>



    </div><!-- #content -->
</div><!-- container -->



<?php get_footer(); ?>
