<footer id="footer" class="footer">
  <div class="container center">
    <div class="row">
      <div class="padding col-6 left">
        <p>©<?php echo date('Y'); ?> - WIJK AAN ZET</p>

      </div>

      <div class="padding col-6 left flex-row">

        <?php if ( has_nav_menu( 'primary' ) ) : ?>

        <nav class="footer-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer disclaimer privacy contact', 'twentysixteen' ); ?>">
          <?php
            wp_nav_menu( array(
              'theme_location' => 'minifooter',
              'menu_class'     => 'minimenu',
             ) );
          ?>
        </nav>

        <?php endif; ?>

  				<a href="http://pharmeon.nl" target="_blank" class="powered">- &nbsp; Powered by Pharmeon ©</a>

      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAjIsxkd03K9rNnrQYS77x0XQB_rWUN0mE"></script>

<script type="text/javascript">
(function($) {

/*
*  render_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function render_map( $el ) {

// var
var $markers = $el.find('.marker');

// vars
var args = {
	zoom		: 14,
	center		: new google.maps.LatLng(0, 0),
	mapTypeId	: google.maps.MapTypeId.ROADMAP,
	scrollwheel : false,

};

// create map
var map = new google.maps.Map( $el[0], args);

// add a markers reference
map.markers = [];




// add markers
$markers.each(function(){

	add_marker( $(this), map );

});


// polygon wijk segbroek
var segbroekCoords = [
  {lat: 52.08597, lng: 4.26518}, {lat: 52.08356, lng: 4.25954}, {lat: 52.08426, lng: 4.25843}, {lat: 52.08527, lng: 4.25728}, {lat: 52.08398, lng: 4.25533}, {lat: 52.08336, lng: 4.25551}, {lat: 52.07878, lng: 4.24566}, {lat: 52.07777, lng: 4.24649}, {lat: 52.07698, lng: 4.24627}, {lat: 52.07642, lng: 4.24538}, {lat: 52.07604, lng: 4.24421}, {lat: 52.0759, lng: 4.24341}, {lat: 52.07591, lng: 4.24237}, {lat: 52.07629, lng: 4.2415}, {lat: 52.07679, lng: 4.2409}, {lat: 52.07696, lng: 4.24025}, {lat: 52.07613, lng: 4.23778}, {lat: 52.07381, lng: 4.23964}, {lat: 52.07323, lng: 4.23803}, {lat: 52.07262, lng: 4.23659}, {lat: 52.07153, lng: 4.23565}, {lat: 52.07022, lng: 4.23295}, {lat: 52.06911, lng: 4.23156}, {lat: 52.06769, lng: 4.23271}, {lat: 52.06687, lng: 4.23358}, {lat: 52.06607, lng: 4.23346}, {lat: 52.06421, lng: 4.23497}, {lat: 52.06748, lng: 4.2463}, {lat: 52.0673, lng: 4.24812}, {lat: 52.06623, lng: 4.25049}, {lat: 52.06545, lng: 4.25334}, {lat: 52.07025, lng: 4.26795}, {lat: 52.06691, lng: 4.27043}, {lat: 52.07071, lng: 4.28467}, {lat: 52.07867, lng: 4.27733}, {lat: 52.07462, lng: 4.26576}, {lat: 52.07933, lng: 4.26131}, {lat: 52.08335, lng: 4.26771},
];

var segbroek = new google.maps.Polygon({
	paths: segbroekCoords,
	strokeColor: '#FF5500',
	strokeOpacity: 0.9,
	strokeWeight: 1,
	fillColor: '#FF5500',
	fillOpacity: 0.30
});

segbroek.setMap(map);


// center map
center_map( map );


}

// create info window outside of each - then tell that singular infowindow to swap content based on click
var infowindow = new google.maps.InfoWindow({
content		: ''
});

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

// var icon
	var image = '/wp-content/themes/wijkaanzet/images/mapicon.png';

// var
var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

// create marker
var marker = new google.maps.Marker({
	position	: latlng,
	map			: map,
	icon		: image
});

// add to array
map.markers.push( marker );

// if marker contains HTML, add it to an infoWindow
if( $marker.html() )
{




	// show info window when marker is clicked & close other markers
	google.maps.event.addListener(marker, 'click', function() {
		//swap content of that singular infowindow
				infowindow.setContent($marker.html());
		        infowindow.open(map, marker);
	});

	// close info window when map is clicked
	     google.maps.event.addListener(map, 'click', function(event) {
	        if (infowindow) {
	            infowindow.close(); }
			});


}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

// vars
var bounds = new google.maps.LatLngBounds();

// loop through all markers and create bounds
$.each( map.markers, function( i, marker ){

	var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

	bounds.extend( latlng );

});

// only 1 marker?
if( map.markers.length == 1 )
{
	// set center of map
    map.setCenter( bounds.getCenter() );
    map.setZoom( 14 );
}
else
{
	// fit to bounds
	map.fitBounds( bounds );
}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/

$(document).ready(function(){

$('.acf-map').each(function(){

	render_map( $(this) );

});

});

})(jQuery);
</script>


</body>
</html>
