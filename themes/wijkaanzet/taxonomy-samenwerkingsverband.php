<?php
/**
 * Template Name: Zorgverleners search
 *
 */

get_header();
?>

<div id="container" class="container center">

    <div class="content">
        <article>
            <div class="entry-content">
                <?php the_content(); ?>

		            <div class="searchheader">
		            	<?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );  ?>
		            	<h1><?php echo $term->name; ?></h1>
		            </div>
		            <div class="tableholder">
	                <table id="zorgverleners" class="tablesorter">
	                    <?php include 'includes/result_head.php'; ?>
	                    <tbody>
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
							include 'includes/result_list.php';
							endwhile; else: ?>
							<p>Sorry, no posts matched your criteria.</p>
						<?php endif; ?> 

	                    </tbody>
	                </table>
		            </div>
            </div><!-- .entry-content -->
        </article>



    </div><!-- #content -->
</div><!-- container -->

<div class="container center single-praktijk--outer">
      <div class="mapholder padding">
        <div class="row">
          <div class="innermap">
              <h2>WAAR IN DEN HAAG ZUID-WEST</h2>

                <div class="acf-map">
                    
                    <?php
                    
                        //use geocoder to get position

                        $address = get_field('straat_huisnummer').' '.get_field('postcode').' '.get_field('stad');
                        $position = geocode($address);

                        if($position) {
                        $website = get_field('website');
                        $phone = get_field('telefoon_praktijk');
                    ?>


                    <div class="marker" data-lat="<?php echo $position[0]; ?>" data-lng="<?php echo $position[1]; ?>">
                            <p class="address">
                              <strong><a style="font-size: 18px; color: #396F28 !important" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></strong><br/>
                              <?php echo $address; ?><br/>
                              <?php if($phone) echo '<a href="tel:'.$phone.'">'.$phone.'</a>'; ?> <?php if($website) { if($phone) echo ' | '; echo '<a href="'.$website.'">'.$website.'</a>'; } ?>
                            </p>
                    </div>

                    <?php
                        }
                        wp_reset_query();
                    ?>
                </div><!-- .acf-map -->
          </div>
        </div>
      </div>
    </div>



<?php get_footer(); ?>
