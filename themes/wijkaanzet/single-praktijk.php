<?php get_header(); ?>


  <?php while ( have_posts() ) : the_post(); ?>

    <div class="container center">
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>zorgverleners"><span class="backbutton">Terug</span></a>

        <div class="content single_zorgverlener">
            <article>
                <div class="entry-content">
                    <div class="searchheader">
                        <h1><?php the_field('naam_zorgpraktijk_of_zorginstelling'); ?></h1>
                        <!-- <table>
                            <tr>
                                <td>
                                    <h3>Type zorginstelling: <span><?php the_field('type_zorginstelling'); ?></span></h3>
                                </td>
                                <td>
                                    <h3>Praktijk: <span>xxx</span></h3>
                                </td>
                                <td>
                                    <h3>Specialisme: <span>xxx</span></h3>
                                </td>
                            </tr>
                        </table> -->
                    </div>
                    <div class="colholder sameheight">
                       <div id="zorgverleners" class="zorginstelling-intro padding">
                           <p>Type zorginstelling: <span><?php the_field('type_zorginstelling'); ?></span></p>
                       </div>
                    </div>
                    <div class="colholder sameheight">
                        <div class="col-6 col-t-12 left padding">
                            <div class="info">
                                <?php if(get_field('naam_zorgpraktijk_of_zorginstelling')): ?>
                                  <p><strong>NAAM </strong><?php the_field('naam_zorgpraktijk_of_zorginstelling'); ?></p>
                                <?php endif; ?>
                                <?php if(get_field('straat_huisnummer') || get_field('postcode') || get_field('stad')): ?>
                                  <p><strong>ADRES </strong>
                                    <?php the_field('straat_huisnummer'); ?><br />
                                    <?php the_field('postcode'); ?><br />
                                    <?php the_field('stad'); ?><br />
                                  </p>
                                <?php endif; ?>
                                <?php if(get_field('website')): ?>

                                  <?php $link = addhttp( get_field('website') ); ?>

                                  <p><strong>WEBSITE </strong> <a href="<?php echo $link ?>" target="_blank"><?php the_field('website'); ?></a></p>

                                <?php endif; ?>
                                <?php if(get_field('email')): ?>
                                  <p><strong>E-MAIL </strong><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>
                                <?php endif; ?>
                                <?php if(get_field('telefoon_praktijk')): ?>
                                  <p><strong>TELEFOON </strong><?php the_field('telefoon_praktijk'); ?></p>
                                <?php endif; ?>
                                <?php if(get_field('telefoon_bij_spoedgevallen')): ?>
                                  <p><strong>TELEFOON PRAKTIJK BIJ SPOEDGEVALLEN </strong><?php the_field('telefoon_bij_spoedgevallen'); ?></p>
                                <?php endif; ?>
                                <?php if(get_field('receptenlijn')): ?>
                                  <p><strong>RECEPTENLIJN </strong><?php the_field('receptenlijn'); ?></p>
                                <?php endif; ?>
                                <?php if(get_field('fax')): ?>
                                  <p><strong>FAX </strong><?php the_field('fax'); ?></p>
                                <?php endif; ?>
                                <?php if(get_field('neemt_nog_patienten_aan')): ?>
                                  <p><strong>NEEMT NOG PATI&Euml;NTEN AAN </strong>
                                    <?php
                                      $patienten = get_field('neemt_nog_patienten_aan');
                                      if ($patienten == 'ja') {
                                          echo '<i class="ja fa fa-check"></i> Ja';
                                      } else {
                                          echo '<i class="nee fa fa-check"></i> Nee';
                                      }
                                    ?>
                                  </p>
                                <?php endif; ?>

                                <?php if(get_field('profiel_praktijk')): ?>
                                  <p><strong>PROFIEL ZORGINSTELLING </strong><div class="profiel_praktijk"><?php the_field('profiel_praktijk'); ?></div></p>
                                <?php endif; ?>

                                <?php if(get_field('type_zorginstelling')): ?>
                                  <p><strong>TYPE ZORGINSTELLING </strong>
                                  <?php if(get_field('type_zorginstelling') === 'Overig'): ?>
                                    OVERIG: <?php the_field('overige_zorginstelling'); ?>
                                  <?php else: ?>
                                    <?php the_field('type_zorginstelling'); ?>
                                  <?php endif; ?>
                                  </p>
                                <?php endif; ?>

                                <?php if(get_field('samenwerkingsverband')): ?>
                                  <?php $term = get_field('samenwerkingsverband'); $ID = get_the_ID(); $term_list = wp_get_object_terms($ID, 'samenwerkingsverband'); ?>
                                  <p><strong>SAMENWERKINGSVERBAND</strong>
                                    <?php foreach($term_list as $term_single) {
                                      echo '<a href="'.get_term_link($term).'">'.$term_single->name.'</a>';
                                    } ?>
                                  </p>
                                <?php endif; ?>



                            </div>
                        </div>

                        <?php if(have_rows('zorgverleners')): ?>
                          <div class="col-6 col-t-12 left padding praktijk-info">
                              <div class="content">
                                <h3>PRAKTIJK INFORMATIE</h3>
                                <?php the_content(); ?>
                              </div>
                              <div class="zorgverleners">
                                  <h3>ZORGVERLENERS IN DE PRAKTIJK</h3>
                                  <table id="zorgverleners">
                                      <thead>
                                          <tr>
                                              <td>Naam</td>
                                              <td>Beroep</td>
                                              <td>Specialismes</td>
                                          </tr>
                                      </thead>
                                      <tbody>
                                        <?php while( have_rows('zorgverleners') ): the_row();
                                            $name = get_sub_field('naam_openbaar');
                                            $lastname = explode(' ',$name);
                                            $lastname = $lastname[count($lastname) -1];
                                            $slug = sanitize_title($lastname);

                                            $uri = explode('?',$_SERVER['REQUEST_URI']);
                                            $uri = $uri[0];

                                            $url = $uri.$slug;
                                          ?>

                                          <tr>
                                            <td>
                                              <a href="<?php echo $url; ?>"><?php echo $name;  ?></a>
                                            </td>
                                            <td>
                                              <?php the_sub_field('beroep'); ?><br />
                                              <?php the_sub_field('overig_beroep'); ?>
                                            </td>
                                            <td>
                                              <?php $beroep = get_sub_field('beroep'); ?>

                                               <?php if ($beroep == 'Ergotherapeut') {
                                                the_sub_field('ergotherapeut');
                                               } elseif ($beroep == 'Fysiotherapeut') {
                                                the_sub_field('fysiotherapeut');
                                               } elseif ($beroep == 'Logopedist') {
                                                the_sub_field('logopedist');
                                              } elseif ($beroep == 'Oefentherapeut') {
                                                the_sub_field('oefentherapeut');
                                               } elseif ($beroep == 'Psychiater') {
                                                the_sub_field('psychiater');
                                               } elseif ($beroep == 'Psycholoog') {
                                                the_sub_field('psycholoog');
                                               } elseif ($beroep == 'Psychotherapeut') {
                                                the_sub_field('psychoterapeut');
                                               } elseif ($beroep == 'Tandarts') {
                                                the_sub_field('tandarts');
                                               }


                                               ?>
                                            </td>
                                          </tr>

                                      	<?php endwhile; ?>
                                      </tbody>
                                  </table>
                              </div>
                              <?php if(get_field('samenwerkingsverband')): ?>
                              <div class="samenwerkingsverband">
                                <h3>PRAKTIJKEN SAMENWERKINGSVERBAND</h3>
                                <table id="samenwerkingsverband">
                                  <thead>
                                    <tr>
                                      <td>Praktijken</td>
                                    </tr>
                                  </thead>
                                  <tbody>



                                          <?php $term = get_field('samenwerkingsverband'); $ID = get_the_ID(); $term_list = wp_get_object_terms($ID, 'samenwerkingsverband'); ?>

                                          <?php
                                        $categories = wp_get_post_terms( $ID, 'samenwerkingsverband');
                                        foreach ( $categories as $cat ) {

                                        $posts_array = get_posts(
                                            array(
                                                'posts_per_page' => -1,
                                                'post_type' => 'praktijk',
                                                'tax_query' => array(
                                                    array(
                                                        'taxonomy' => 'samenwerkingsverband',
                                                        'field' => 'term_id',
                                                        'terms' => $cat->term_id,
                                                    )
                                                )
                                            )
                                        ); ?>
                                            <?php foreach ( $posts_array as $post ) : setup_postdata( $post ); ?>
                                                <tr><td> <a href="<?php the_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a> </td></tr>
                                            <?php endforeach;
                                            wp_reset_postdata();?> <?php
                                         }
                                        ?>


                                  </tbody>
                                </table>
                              </div>
                              <?php endif; ?>
                          </div>
                        <?php endif; ?>
                    </div>
                </div><!-- .entry-content -->
            </article>
        </div><!-- #content -->
    </div>



    <div class="container center single-praktijk--outer">
      <div class="mapholder padding">
        <div class="row">
          <div class="innermap">
              <h2>WAAR IN DEN HAAG ZUID-WEST</h2>

                <div class="acf-map">

                    <?php

                        //use geocoder to get position

                        $address = get_field('straat_huisnummer').' '.get_field('postcode').' '.get_field('stad');
                        $position = geocode($address);

                        if($position) {
                        $website = get_field('website');
                        $phone = get_field('telefoon_praktijk');
                    ?>


                    <div class="marker" data-lat="<?php echo $position[0]; ?>" data-lng="<?php echo $position[1]; ?>">
                            <p class="address">
                              <strong><a style="font-size: 18px; color: #396F28 !important" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></strong><br/>
                              <?php echo $address; ?><br/>
                              <?php if($phone) echo '<a href="tel:'.$phone.'">'.$phone.'</a>'; ?> <?php if($website) { if($phone) echo ' | '; echo '<a href="'.$website.'">'.$website.'</a>'; } ?>
                            </p>
                    </div>

                    <?php
                        }
                        wp_reset_query();
                    ?>
                </div><!-- .acf-map -->
          </div>
        </div>
      </div>
    </div>



  <?php endwhile; ?>


<?php get_footer(); ?>
