jQuery(function($){

  $('.menu-toggle').click(function(){

    if($('body').hasClass('menu-toggled')) $('body').removeClass('menu-toggled');
    else $('body').addClass('menu-toggled');
  })


  $('#stype').change(function(){
    var type = $(this).val();
    $("#subtype").attr("disabled", 'disabled');
    $("#subtype").empty();
    $.ajax({
      url: "/wp-admin/admin-ajax.php",
      type : "post",
      //dataType : "json",
      data : {type: type, action: 'types'},
      success: function(results) {
        $("#subtype").removeAttr("disabled");
        $("#subtype").append(results);
      }
    })
  });

  // search tabs
  $(".specialisme").click(
    function () {
      $(".specialismezoeken").addClass('on');
      $(".keywordzoeken").removeClass('on');
      $(".praktijkzoeken").removeClass('on');
      $(".specialisme").addClass('on');
      $(".praktijksoort").removeClass('on');
      $(".keyword").removeClass('on');

    });

  $(".praktijksoort").click(
    function () {
      $(".specialismezoeken").removeClass('on');
      $(".keywordzoeken").removeClass('on');
      $(".praktijkzoeken").addClass('on');
      $(".specialisme").removeClass('on');
      $(".praktijksoort").addClass('on');
      $(".keyword").removeClass('on');
    });
  $(".keyword").click(
    function () {
      $(".specialismezoeken").removeClass('on');
      $(".keywordzoeken").addClass('on');
      $(".praktijkzoeken").removeClass('on');
      $(".specialisme").removeClass('on');
      $(".praktijksoort").removeClass('on');
      $(".keyword").addClass('on');
    });

  // fancybox
  //$('.fancybox').fancybox();

  $('div.entry-content').each(function(i) {
    $('.size-medium').parents('a').addClass('fancybox');
    $('.size-medium').parents('a').attr('rel', 'Wijk aan zet');
    $('.size-large').parents('a').addClass('fancybox');
    $('.size-large').parents('a').attr('rel', 'Wijk aan zet');
    $('.size-thumbnail').parents('a').addClass('fancybox');
    $('.size-thumbnail').parents('a').attr('rel', 'Wijk aan zet');
    $('.size-full').parents('a').addClass('fancybox');
    $('.size-full').parents('a').attr('rel', 'Wijk aan zet');
  });

  $('#zorgverleners').tablesorter();


  if($('.form--practice').length) {
    $('#acf-field_584a82599a630, #acf-field_584a82689a631, #acf-field_584a82939a632, #acf-field_584a82939a634, #acf-field_584a82939a635').keyup(prefill);
    $('.acf-repeater .acf-btn').click(prefill);

    prefill();

    function prefill() {
      $('.acf-field-584a8984b79f3 input').val(
        $('#acf-field_584a82599a630').val() + ', ' + $('#acf-field_584a82689a631').val() + ' ' + $('#acf-field_584a82939a632').val()
      );

      $('.acf-field-584a8999b79f4 input, .acf-field-584a89c3b79f7 input').val($('#acf-field_584a82aa9a635').val());

      $('.acf-field-584a89a0b79f5 input, .acf-field-584a89d4b79f8 input').val($('#acf-field_584a82a59a634').val());
    }
  }
});
