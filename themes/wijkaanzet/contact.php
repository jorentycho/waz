<?php
/**
 * Template Name: Contact
 *
 */

get_header();
?>
<div id="container" class="container container--home center">

    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <div class="content">

        <article class="col-6 col-t-12 left content--text">
            <header>
                <h1><?php the_title(); ?></h1>
            </header>
            <div class="entry-content">
                <?php the_content(); ?>
            </div><!-- .entry-content -->
        </article>


        <?php endwhile; // end of the loop. ?>

        <div class="metabox col-6 col-t-12 left">
            <?php include 'includes/searchbox.php'; ?>



            <div class="latestnews">
                <h2><i class="fa fa-newspaper-o"></i> NIEUWS</h2>

                <?php
        					query_posts( array ( 'showposts' => 3, 'orderby' => 'date', 'order'=>DESC ) );
        					while ( have_posts() ) : the_post();
        				?>
                <a class="news" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                <?php
        					endwhile;
        					wp_reset_query();
        				?>
                <a class="meernews" href="<?php bloginfo('url'); ?>/nieuws/">Meer nieuws</a>

                <div style="clear:both;"></div>
            </div>

        </div>
        <div class="clearfix"></div>




        <div style="clear:both;"></div>


        	<?php include 'includes/map.php'; ?>
        </div>



    </div><!-- #content -->
</div><!-- container -->





<?php get_footer(); ?>
