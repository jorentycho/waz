<?php
/**
 * Template Name: Nieuwsoverzicht
 *
 */

get_header();
?>
<div id="container" class="container center">

    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <div class="content">
        <article class="col-6 col-t-12 padding">
            <header class="nieuws-intro">
                <h1><?php the_title(); ?></h1>
            </header>
            <div class="entry-content">
                <?php the_content(); ?>
            </div><!-- .entry-content -->
        </article>

    <?php endwhile; // end of the loop. ?>


    <div style="clear:both;"></div>
    <div class="col-12 nieuwsoverzicht">
    <?php

              $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

              $custom_args = array(
                  'post_type' => 'post',
                  'posts_per_page' => -1,
                  'paged' => $paged
                );

              $custom_query = new WP_Query( $custom_args ); ?>

              <?php if ( $custom_query->have_posts() ) : ?>

                <!-- the loop -->
                <?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>


                        <div class="col-12 col-t-12 left padding nieuws-single">
                            <a href="<?php the_permalink(); ?>" class="nieuwsbg col-2 col-d-4 col-m-0" style="background: url(<?php the_post_thumbnail_url(); ?>);"></a>
                            <div class="nieuwscontent col-10 col-d-8 col-m-12">
                                <h3 class="nieuwscontent--title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                <p class="news-date"><?php the_time('j F Y');?></p>
                                <p>
                                <?php

                                    $content = get_the_content();
                                    echo wp_trim_words( $content , '60' );

                                ?>   <a href="<?php the_permalink() ?>">
                                  Lees meer...
                                </a>
                                </p>
                                
                            </div>
                        </div>

                <?php endwhile; ?>
                <!-- end of the loop -->
            <div class="clearfix"></div>
              <?php wp_reset_postdata(); ?>

            <?php else: ?>

              <tr>
                <td colspan="6">
                  <h4>Helaas, geen resultaten</h4>
                </td>
              </tr>


              <?php endif; ?>



            </div>
            <?php include "includes/map.php"; ?>
    </div><!-- #content -->


</div><!-- container -->










<?php get_footer(); ?>
