<?php get_header(); ?>

	<div id="primary">
		<div id="content" class="content" role="main">
			<div class="container center padding">
				<div class="row">

			<?php while ( have_posts() ) : the_post(); ?>

				
						<div class="content--text">
							<h1><?php the_title(); ?></h1>
							<p class="news-date__single"><?php the_time('j F Y');?></p>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>nieuws" class="news__back">Terug naar nieuwsoverzicht</a>
						</div>
			            <div class="columns padding">
			              <?php the_content(); ?>
			            </div>

				<?php
          endwhile; ?>
          		</div>
				
				<div style="clear:both;"></div>
				<div class="news__single--map">
		        	<?php include 'includes/map.php'; ?>
		        </div>
			</div>

        </div>
    </div>

<?php get_footer(); ?>
